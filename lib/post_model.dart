import 'package:flutter/foundation.dart' show required;

class Post {
  final int id;
  final String name;
  final String price;
  final String about;

  Post({
    @required this.id,
    @required this.name,
    @required this.price,
    @required this.about,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json["id"] as int,
      name: json["name"] as String,
      price: json["price"] as String,
      about: json["about"] as String,
    );
  }

  String get title => null;
}
