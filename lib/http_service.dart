import 'dart:convert';
import 'package:fghjk/post_model.dart';
import 'package:http/http.dart';

class HttpService {
  get postsUrl => "https://192.168.1.6:3000/";

  Future<List<Post>> getPosts() async {
    Response res = await get(postsUrl);

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<Post> posts =
          body.map((dynamic item) => Post.fromJson(item)).toList();

      return posts;
    } else {
      throw "Cant get posts.";
    }
  }
}
